Feature: Request on MockServer

  Scenario: Send a request on MockServer
    Given Wiremock on port 8090  on Post request on "/hello" return a message with "hello" body
    When I send a request
    Then response body should  include "hello"

