package stepDefinitions;

import com.github.tomakehurst.wiremock.WireMockServer;
import io.restassured.response.Response;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Container {

    private WireMockServer wireMockServer;
    private String requestURL;
    private Response response;
    private int wireMockPortNumber;
}
