package stepDefinitions;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.common.ConsoleNotifier;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.extension.responsetemplating.ResponseTemplateTransformer;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.hamcrest.Matchers;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static io.restassured.RestAssured.when;

public class Steps {
    private static final Container container = new Container();

    /**
     * Create an Mock on defined port on defined path with text
     *
     * @param portNumber  port Number
     * @param requestPath request  path
     * @param bodyText    body text
     */
    @Given("Wiremock on port {int}  on Post request on {string} return a message with {string} body")
    public void wiremock_on_port_on_Post_request_on_return_a_message_with_body(Integer portNumber, String requestPath, String bodyText) {

        container.setRequestURL(requestPath);
        container.setWireMockPortNumber(portNumber);
        container.setWireMockServer(new WireMockServer(WireMockConfiguration.options().port(portNumber)
                .notifier(new ConsoleNotifier(true)).extensions(new ResponseTemplateTransformer(true))));
        container.getWireMockServer().start();
        container.getWireMockServer().stubFor(get(urlMatching(requestPath)).willReturn(
                aResponse().withStatus(200).withHeader("Content-Type", "text/xml; charset=UTF-8")
                        .withBody(bodyText)));
    }


    @When("I send a request")
    public void i_send_a_request() {
        container.setResponse(when().get("http://localhost:" +
                container.getWireMockPortNumber() +
                container.getRequestURL()));
    }

    @Then("response body should  include {string}")
    public void response_body_should_include(String body) {
        container.getResponse().then().statusCode(200)
                .log().all();

        container.getResponse().then().body( Matchers.containsString(body));
        container.getWireMockServer().stop();
    }

}
