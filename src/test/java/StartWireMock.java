import com.github.tomakehurst.wiremock.WireMockServer;
import io.restassured.response.Response;


import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static io.restassured.RestAssured.when;

public class StartWireMock {


    public static void main(String[] args) {


        WireMockServer wireMockServer = new WireMockServer(8090);


        wireMockServer.start();
        wireMockServer.stubFor(get(urlMatching("/mockTest")).willReturn(
                aResponse().withStatus(200).withHeader("Content-Type", "text/xml; charset=UTF-8")
                        .withBody("Hi from WireMock")));

        Response response = when().get("http://localhost:8090/mockTest");
        System.out.println(response.asString());

        wireMockServer.stop();


    }
}

